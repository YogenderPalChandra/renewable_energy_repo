###################################
##importing essential libraries
###################################
import os
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import matplotlib.colors as clrs
import pandas as pd
import math
import datetime
import pathlib
########################################
##Path to files
#######################################

##pathlib = list(pathlib.Path("D:\Python\CrossTemProfile60105010\6010/").rglob("ALMEMO*"))

data_dir_temProfile = 'D:\\Python\\RichardsonNumber\\6010//'
litersPerHour = ['800', '600', '400','200']
filepaths = [data_dir + 'ALMEMO' + x + '.001'for x in litersPerHour]
filepaths_temProfile = [data_dir_temProfile + 'ALMEMO' + x + '.001'for x in litersPerHour]

colors = ['m', 'g', 'r', 'k']
colors_temProfile  = ['m', 'g', 'r', 'k']
listMarkers = ["+","o","v","^","<",">","s","p","*","h","x","D","d","1","2","3","4","d","|","X"]
#listMarkers_temProfile = [ "*","h","x","D","d","1","2","3","4","d","|","X"]
Temlegends = ["1","2","3","4","5","6","7","8", "9","10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20"]
reversedTemlegends = Temlegends[::-1]
#TemProfileLitersPerHour = ['800l/h', '600l/h', '200l/h']
TemProfileLitersPerHour = [x +"l/h" for x in litersPerHour]

listColors = ["b","g","r","c","m","y","k","b","g","r","c","m","y","k","b","g","r","c","m","y"]

pointtypes = ['-.', '-.', '-.', '-.']
markers = ['+','x', 'o', 'v']
markers_temProfile = [ "X","h","4","D"]
labels = ['800l/h (60-20)', '600l/h (60-20)', '400l/h (60-20)', '200l/h (60-20)']
labels_temProfile = ['800l/h (60-10)', '600l/h (60-10)', '400l/h (60-10)', '200l/h (60-10)']
out_dir = 'D:\\Python\\RichardsonNumber//'
out_dirs = [out_dir + x + "l_h" for x in litersPerHour]

os.makedirs(out_dir, exist_ok = True)


############
###Function to read data set
############
def read_data_temProfile(fpath):
    if fpath == filepaths_temProfile[0]:
        df = pd.read_csv(os.path.join(fpath), sep = ";", skiprows = 40, header = None)
    elif fpath == filepaths_temProfile[1]:
        df = pd.read_csv(os.path.join(fpath), sep = ";", skiprows = 40, header = None)
    elif fpath == filepaths_temProfile[2]:
        df = pd.read_csv(os.path.join(fpath), sep = ";", skiprows = 40, header = None)
    elif fpath == filepaths_temProfile[3]:
        df = pd.read_csv(os.path.join(fpath), sep = ";", skiprows = 1356, header = None)
        
    return df
############################################
##Function to read filepaths as a list. And store them in a list. So its a list of pandas dataframe.
##Noteworthy point is that every dataframe is stored in a list so I have to enter a list to perform pandas functions. 
############################################
def path2df_temProfile(paths):        
        dfs = [read_data_temProfile(p) for p in paths]
        return dfs
    
Dfs_temProfile = path2df_temProfile(filepaths_temProfile)

###############################################
##Function to read the temperature values to analyze. I is the list of range of coloums to slice from data.
###I is using list() whic converts the iteratable sych as tuples, sets dictionaries to list.iloc is pandas slicing. So I have
#### looped inside the list to enter the pandas dataframe to perform iloc function.
##############################################
def Tlow_temProfile(dfs):
        #I = list(range(62, 72)) + list(range(82, 92))
        dataframe = [df.iloc[:, 4] for df in dfs]
        return dataframe
Tlow_lowerConnection_temProfile = Tlow_temProfile(Dfs_temProfile)
#print ('Tem values to code upone = ', Tem2Code)
#print (type(Tem2Code))

def Ttop_temProfile(dfs):
        #I = list(range(62, 72)) + list(range(82, 92))
        dataframe = [df.iloc[:, 5] for df in dfs]
        return dataframe
Ttop_upperConnection_temProfile = Ttop_temProfile(Dfs_temProfile)
#print ('Tem values to code upone = ', Tem2Code)
#print (type(Tem2Code))

def Tlow_seriesTodf_temProfile(dfs):
    dataFrame = [df.rename(None).to_frame() for df in dfs]
    return dataFrame
seriesTodf_lower_ToReplaceComa_temProfile = Tlow_seriesTodf_temProfile(Tlow_lowerConnection_temProfile)
##for i in seriesTodf_ToReplaceComa:
##    print (type(i))

def Ttop_seriesTodf_temProfile(dfs):
    dataFrame = [df.rename(None).to_frame() for df in dfs]
    return dataFrame
seriesTodf_upper_ToReplaceComa_temProfile = Ttop_seriesTodf_temProfile(Ttop_upperConnection_temProfile)
##for i in seriesTodf_ToReplaceComa:
##    print (type(i))
######################################
##Function to replace commas with dot as it will later be converted to float. with comma it cannot be converted to float. 
##I dont know how df.stack and df.unstack works, but the matter of fact is it worked. Google for more info.
######################################
def Tlow_replace_comma_temProfile(dfs):
    val = [df.stack().str.replace(',', '.').unstack() for df in dfs]
    return val
Tlow_comma2Dot_temProfile = Tlow_replace_comma_temProfile(seriesTodf_lower_ToReplaceComa_temProfile)
#print (comma2Dot)
#print (type(comma2Dot))

def Ttop_replace_comma_temProfile(dfs):
    val = [df.stack().str.replace(',', '.').unstack() for df in dfs]
    return val

Ttop_comma2Dot_temProfile = Ttop_replace_comma_temProfile(seriesTodf_upper_ToReplaceComa_temProfile)
#print (comma2Dot)
#print (type(comma2Dot))
###################################
##Conversion of string to float. It is somtimes necessary to convert pandas data to float. Just in case its a good practise
##################################

def Tlow_floatdata_temProfile(dfs):
    val = [df.applymap(float) for df in dfs]
    return val
Tlow_string2float_temProfile = Tlow_floatdata_temProfile(Tlow_comma2Dot_temProfile)
#print (string2float)

def Ttop_floatdata_temProfile(dfs):
    val = [df.applymap(float) for df in dfs]
    return val
Ttop_string2float_temProfile = Ttop_floatdata(Ttop_comma2Dot_temProfile)
#print (string2float)

def TtopNegativeTlow_temProfile(A,B):
    val = [a-b for a,b in zip (A,B)]
    return val
TtopMinusTlow_temProfile = TtopNegativeTlow_temProfile(Ttop_string2float_temProfile, Tlow_string2float_temProfile)

def RichardsonNumber_temProfile(dfs):
    g = 9.8
    B = 0.000522
    H = 1.345
    v = 0.04879
    Ri = [(g*B*H*df)/v**2 for df in dfs]
    return Ri
RichardsonNumber_list_temProfile = RichardsonNumber_temProfile(TtopMinusTlow_temProfile)
#print (type((RichardsonNumber_list_temProfile))
##len(RichardsonNumber_list_temProfile)
##for i in RichardsonNumber_list_temProfile:
##       print (type (i))


############################################
##Function to retrieve the date string from DFs
############################################
 
def dateString_l_of_dfs_temProfile(dfs):
    #I = list(range(20,300))
    #I = list(range(1140,1476))
    datestring = [df.iloc[:,1]for df in dfs]
    return datestring
dates_list_of_Dfs_temProfile = dateString_l_of_dfs_temProfile(Dfs_temProfile)
##print ('date =',  dates_list_of_Dfs)
###print (type(Dfs))
##print (np.shape(dates_list_of_Dfs))
##print (len(dates_list_of_Dfs))
##print (type(dates_list_of_Dfs))

#################################
##Function to parse date string as datetime object by (datetime.datetime.strptime(x, '%H:%M:%S'))
##and then by performing (.time ) in end
#################################
def datestring_to_datetime_temProfile(datestr):
    val = [[datetime.datetime.strptime(x, '%H:%M:%S').time() for x in y ]for y in datestr]    
    return val
dateString2dateTime_temProfile = datestring_to_datetime_temProfile(dates_list_of_Dfs_temProfile)

################################
##Function to perfrom nondimensionalisation of time. Note that I have used pd.to_datime to convert the date time series in
##dateString2dateTime to pandas date time series so I could easily perform subtraction and division of timestamp and timedelta object
################################

def scale_it_pseries_temProfile(pseries):
    timString2PdTime=[pd.to_datetime(s,format='%H:%M:%S') for s in pseries]
    denominator = [p.max() - p.min() for p in timString2PdTime]
    nominator = [p - p.min() for p in timString2PdTime]
    scaled_value = [(n/d) for n,d  in zip (nominator,denominator)]
    return scaled_value
timeScaled_temProfile = scale_it_pseries_temProfile(dateString2dateTime_temProfile)
print (timeScaled)

def RichPlotBoth(RichardsonNumber6010, timeScaled6010, RichardsonNumber6020,timeScaled6020, out_dir, colors, colors1,pointtypes,
                labels,
                labels1,
                markers,
                markers1,
                xlabel = "Dimensionless time",
                ylabel = "Richardson Number",
                ylimit =(),
                xlimit = ()):
    plt.figure()
    ttl = plt.title("Richardson number at various flow rate")
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    if ylimit != ():
        plt.ylim(ylimit)
    if xlimit != ():
        plt.xlim(xlimit)
    for i, (Rich_6010, Rich_6020, time_6010, time_6020, color, marker, color1, marker1, label, label1) in enumerate(zip (RichardsonNumber6010,RichardsonNumber6020, timeScaled6010, timeScaled6020,colors, markers, colors1,markers1, labels, labels1)):
        ydata_6010 = Rich_6010
        xdata_6010 = time_6010
        ydata_6020 = Rich_6020
        xdata_6020 = time_6020
        ax= plt.subplot(111)
        plt.plot(xdata_6010, ydata_6010, color1+marker1, label = label1, ms=2.5,markevery=20)
        plt.plot(xdata_6020, ydata_6020, color+marker, label = label, ms=2.5,markevery=20)
        
    xlab = ax.xaxis.get_label()
    ylab = ax.yaxis.get_label()
    xlab.set_style('italic')
    xlab.set_size(10)
    ylab.set_style('italic')
    ylab.set_size(10)
    plt.legend()
    ax.grid('on')
    ttl = ax.title
    ttl.set_weight('bold')
    plt.savefig(out_dir + 'plot60206010' + '.svg', transparent = False, bbox_inches="tight")
    plt.show()
        
        
RichBothplotFigures = RichPlotBoth(RichardsonNumber_list_temProfile, timeScaled_temProfile,RichardsonNumber_list,timeScaled,
                        out_dir,
                        colors,
                        colors_temProfile,
                        pointtypes,
                        labels,
                        labels_temProfile,
                        markers,
                        markers_temProfile,
                        xlabel = "Dimensionless time",
                        ylabel = "Mix number",
                        ylimit =(0,150),
                        xlimit = (0, 1) )


